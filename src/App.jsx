import React from "react";
import {Routing} from "./Routing";
import {Navbar} from "./components/Navbar/Navbar";

export const App = () => {
    return (
        <>
            <Navbar />
            <Routing />
        </>
    )
}