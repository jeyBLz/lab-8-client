import React from "react";
import {AppBar, Box, Button, styled, Toolbar} from "@mui/material";
import {useNavigate} from "react-router-dom";
import logo from '../../img/bahnem.png'
import {StyledButton} from "../StyledButton/StyledButton.jsx";

const StyledImage = styled('img')({
    height: '32px',
    margin: '0 7px 0 0',
    '&:hover': {
        cursor: 'pointer'
    }
})

export const Navbar = () => {
    const navigate = useNavigate();

    return (
        <>
            <Box>
                <Box style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <AppBar position={'sticky'} color={'transparent'} sx={{marginBottom: '10px', width: {sm: '75vw'}}}>
                        <Toolbar sx={{minWidth: '512px', width: '75vw'}}>
                            <StyledImage onClick={() => navigate('/')} src={logo} alt={'logo'}/>
                            <div>
                                <StyledButton
                                    variant={'text'}
                                    color={'inherit'}
                                    onClick={() => navigate('/artists')}
                                    sx={{
                                        textTransform: 'none',
                                        backgroundColor: 'transparent',
                                    }}
                                >Artists</StyledButton>
                                <StyledButton
                                    variant={'text'}
                                    color={'inherit'}
                                    onClick={() => navigate('/albums')}
                                    sx={{
                                        textTransform: 'none',
                                        backgroundColor: 'transparent',
                                    }}
                                >Albums</StyledButton>
                                <StyledButton
                                    variant={'text'}
                                    color={'inherit'}
                                    onClick={() => navigate('/songs')}
                                    sx={{
                                        textTransform: 'none',
                                        backgroundColor: 'transparent',
                                    }}
                                >Songs</StyledButton>
                            </div>
                        </Toolbar>
                    </AppBar>
                </Box>
            </Box>
        </>
    );
}