import React from "react";
import {Button, styled} from "@mui/material";

export const StyledButton = styled(Button)({
    display: 'inline-block',
    marginTop: '5px',
    '&:active': {
        boxShadow: 'transparent',
    },
    '&::after': {
        content: "''",
        display: 'block',
        width: 0,
        height: '2px',
        background: '#1976d2',
        transition: 'width .3s',
    },
    '&:hover::after': {
        width: '100%',
    },
    '&:hover': {
        backgroundColor: 'transparent',
    },
});