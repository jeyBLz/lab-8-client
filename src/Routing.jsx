import React from "react";
import {Navigate, Route, Routes} from "react-router-dom";
import {MainPage} from "./pages/MainPage.jsx";
import {ArtistsPage} from "./pages/ArtistsPage/ArtistsPage.jsx";
import {AlbumsPage} from "./pages/AlbumsPage/AlbumsPage.jsx";
import {SongsPage} from "./pages/SongsPage/SongsPage.jsx";

export const Routing = () => {
    return (
        <>
            <Routes>
                <Route path={'/'} element={<MainPage />} />
                <Route path={'/artists'} element={<ArtistsPage />} />
                <Route path={'/albums'} element={<AlbumsPage />} />
                <Route path={'/songs'} element={<SongsPage />} />

                <Route path={'*'} element={<Navigate to={'/'} />} />
            </Routes>
        </>
    );
}