export const getTimeStringOutOfSeconds = (seconds, isLeadingZerosNeeded = false) => {
    let minutes = Math.floor(seconds / 60);
    let secondsNum = seconds - minutes * 60;

    if (secondsNum < 10) {
        secondsNum = '0' + secondsNum;
    }
    if (isLeadingZerosNeeded && minutes < 10) {
        minutes = '0' + minutes;
    }

    return `${minutes}:${secondsNum}`;
}

export const getSecondsOutOfTimeString = (string) => {
    const values = string.split(':').map(value => parseInt(value));
    return values[0] * 60 + values[1];
}