import React, {useEffect, useState} from "react";
import {
    Autocomplete,
    Box,
    Button,
    ButtonGroup,
    CircularProgress,
    Fade,
    FormControl,
    FormHelperText,
    InputLabel, MenuItem,
    Modal,
    OutlinedInput,
    Paper,
    Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow, TextField
} from "@mui/material";
import {FormBoxStyle, HeaderTypography, StyledBox} from "../style/Styles.js";
import {getTimeStringOutOfSeconds} from "../../service/time-service";

const rowsPerPage = 10;

export const AlbumsPage = () => {
    const [keyForUpdate, setKeyForUpdate] = useState(0);

    const [isLoading, setIsLoading] = useState(true);

    const [albums, setAlbums] = useState([]);
    const [artistNames, setArtistNames] = useState([]);

    const [isModalOpen, setIsModalOpen] = useState(false);
    const handleOpen = () => setIsModalOpen(true);
    const handleClose = () => {
        setIsModalOpen(false);
        setIsNameError(false);
        setIsArtistError(false);
    }

    const [pickedAlbum, setPickedAlbum] = useState({});

    const [pageNumber, setPageNumber] = useState(0);

    const [isNameError, setIsNameError] = useState(false);
    const [isArtistError, setIsArtistError] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        fetch('http://localhost:8080/artists?field=name', {
            method: 'GET'
        })
            .then(response => response.json())
            .then(response => setArtistNames(response))
            .then(() => {
                fetch('http://localhost:8080/albums', {
                    method: 'GET'
                })
                    .then(response => response.json())
                    .then(response => setAlbums(response.sort((a1, a2) => a1.id - a2.id)))
                    .then(() => setIsLoading(false));
            });
    }, [keyForUpdate])

    const handleClick = (album) => {
        setPickedAlbum(album);
    }

    const handleEditClick = (album) => {
        handleClick(album);
        handleOpen();
    }

    const handleDeleteClick = (album) => {
        fetch('http://localhost:8080/albums', {
            method: 'DELETE',
            body: JSON.stringify(album)
        }).then(() => setKeyForUpdate(keyForUpdate => keyForUpdate + 1));
    }

    const handleAddClick = () => {
        handleClick({
            id: '',
            name: '',
            genre: 'Rock',
            artistName: artistNames[0]
        });
        handleOpen();
    }

    const handleNameChange = (event) => {
        pickedAlbum.name = event.target.value;
        setPickedAlbum(pickedAlbum);
        setIsNameError(checkInput(pickedAlbum.name));
    }

    const handleGenreChange = (event) => {
        pickedAlbum['genre'] = event.target.value;
        setPickedAlbum(pickedAlbum);
    }

    const handleArtistChange = (value) => {
        pickedAlbum['artistName'] = value;
        setPickedAlbum(pickedAlbum);
        setIsArtistError(checkInput(pickedAlbum.artistName))
    }

    const checkInputs = () => {
        const name = checkInput(pickedAlbum.name)
        const artist = checkInput(pickedAlbum['artistName']);
        setIsNameError(name);
        setIsArtistError(artist);
        return name || artist;
    }

    const handleSaveClick = () => {
        if (checkInputs()) {
            return;
        }
        fetch('http://localhost:8080/albums', {
            method: pickedAlbum.id ? 'PUT' : 'POST',
            body: JSON.stringify(pickedAlbum)
        }).then(() => {
            handleClose();
            setKeyForUpdate(keyForUpdate => keyForUpdate + 1);
        });
    }

    const checkInput = (value) => {
        const regExp = /^.+$/
        return !value.match(regExp);
    }

    const handlePageChange = (event, newPage) => {
        setPageNumber(newPage);
    }

    if (isLoading) {
        return (
            <CircularProgress
                style={{
                    position: 'fixed',
                    left: '50%',
                    top: '50%'
                }}
            />
        );
    }

    return (
        <>
            <StyledBox>
                <HeaderTypography>Albums</HeaderTypography>
                <TableContainer component={Paper} sx={{width: 'auto'}}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>NAME</TableCell>
                                <TableCell>GENRE</TableCell>
                                <TableCell>ARTIST</TableCell>
                                <TableCell>SONGS</TableCell>
                                <TableCell>DURATION</TableCell>
                                <TableCell>ACTIONS</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {albums.slice(pageNumber * rowsPerPage, pageNumber * rowsPerPage + rowsPerPage)
                                .map((album, index) => {
                                        return (
                                            <TableRow key={album.id}>
                                                <TableCell>{pageNumber * rowsPerPage + index + 1}</TableCell>
                                                <TableCell>{album.name}</TableCell>
                                                <TableCell>{album['genre']}</TableCell>
                                                <TableCell>{album['artistName']}</TableCell>
                                                <TableCell>{album['songs_count']}</TableCell>
                                                <TableCell>{getTimeStringOutOfSeconds(album['total_duration'])}</TableCell>
                                                <TableCell>
                                                    <ButtonGroup>
                                                        <Button
                                                            onClick={() => handleEditClick(structuredClone(album))}
                                                            variant={'outlined'}
                                                            color={'primary'}
                                                        >Edit</Button>
                                                        <Button
                                                            onClick={() => handleDeleteClick(album)}
                                                            variant={'outlined'}
                                                            color={'error'}
                                                        >Delete</Button>
                                                    </ButtonGroup>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    }
                                )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TableCell colSpan={5}>
                                    <Button
                                        onClick={handleAddClick}
                                        variant={'outlined'}
                                        color={'success'}
                                    >Add</Button>
                                </TableCell>
                                <TablePagination
                                    rowsPerPageOptions={[rowsPerPage]}
                                    count={albums.length}
                                    page={pageNumber}
                                    rowsPerPage={rowsPerPage}
                                    onPageChange={(event, page) => handlePageChange(event, page)}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
                <Modal
                    open={isModalOpen}
                    onClose={handleClose}
                    closeAfterTransition
                >
                    <Fade in={isModalOpen}>
                        <Box sx={FormBoxStyle}>
                            <FormControl error={isNameError} variant={'outlined'} sx={{margin: '10px 0'}}>
                                <InputLabel htmlFor={'name-input'}>Name</InputLabel>
                                <OutlinedInput
                                    id={'name-input'}
                                    label={'Name'}
                                    name={'name-input'}
                                    onChange={handleNameChange}
                                    aria-describedby={'name-input-error-text'}
                                    defaultValue={pickedAlbum.name}
                                />
                                <FormHelperText
                                    sx={{display: isNameError ? 'block' : 'none'}}
                                    id={'name-input-error-text'}
                                    color={'error'}
                                >The name is incorrect</FormHelperText>
                            </FormControl>
                            <FormControl variant={'outlined'} sx={{margin: '10px 0', width: '100%'}}>
                                <InputLabel htmlFor={'genre-input'}>Genre</InputLabel>
                                <Select
                                    id={'genre-input'}
                                    label={'Genre'}
                                    name={'genre-input'}
                                    onChange={handleGenreChange}
                                    defaultValue={pickedAlbum['genre']}
                                >
                                    <MenuItem value={'Blues'}>Blues</MenuItem>
                                    <MenuItem value={'Chanson'}>Chanson</MenuItem>
                                    <MenuItem value={'Country'}>Country</MenuItem>
                                    <MenuItem value={'Electronic music'}>Electronic music</MenuItem>
                                    <MenuItem value={'Epic Metal'}>Epic Metal</MenuItem>
                                    <MenuItem value={'Hip-Hop'}>Hip-Hop</MenuItem>
                                    <MenuItem value={'Jazz'}>Jazz</MenuItem>
                                    <MenuItem value={'Rap'}>Rap</MenuItem>
                                    <MenuItem value={'Rock'}>Rock</MenuItem>
                                    <MenuItem value={'Rock-n-roll'}>Rock-n-roll</MenuItem>
                                    <MenuItem value={'R&B'}>R&B</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl error={isArtistError} variant={'outlined'} sx={{margin: '10px 0', width: '100%'}}>
                                <Autocomplete
                                    defaultChecked={isModalOpen}
                                    autoHighlight
                                    aria-describedby={'artist-input-error-text'}
                                    defaultValue={pickedAlbum['artistName']}
                                    renderInput={(params) => <TextField error={isArtistError} {...params} label={'Artist'} />}
                                    options={artistNames}
                                    onChange={(event, newValue) => handleArtistChange(newValue)}
                                />
                                <FormHelperText
                                    sx={{display: isArtistError ? 'block' : 'none'}}
                                    id={'artist-input-error-text'}
                                    color={'error'}
                                >The artist is incorrect</FormHelperText>
                            </FormControl>
                            <Button onClick={handleSaveClick} variant={'outlined'} color={'success'} sx={{width: '25%'}}>Save</Button>
                        </Box>
                    </Fade>
                </Modal>
            </StyledBox>
        </>
    );
}