import {Box, styled, Typography} from "@mui/material";

export const StyledBox = styled(Box)({
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
});

export const HeaderTypography = styled(Typography)({
    fontSize: '36px'
});

export const FormBoxStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 'auto',
    backgroundColor: 'background.paper',
    borderRadius: '5px',
    boxShadow: 10,
    p: 4,
};