import React from "react";
import hello from '../img/lab8.png'

export const MainPage = () => {
    return (
        <>
            <img src={hello} alt={'hello'} style={{height: '90vh'}}/>
        </>
    )
}