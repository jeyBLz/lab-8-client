import React, {useEffect, useState} from "react";
import {
    Autocomplete,
    Box,
    Button,
    ButtonGroup,
    CircularProgress,
    Fade,
    FormControl,
    FormHelperText,
    InputLabel,
    Modal,
    OutlinedInput,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TextField
} from "@mui/material";
import {FormBoxStyle, HeaderTypography, StyledBox} from "../style/Styles.js";
import {getSecondsOutOfTimeString, getTimeStringOutOfSeconds} from "../../service/time-service";

const rowsPerPage = 10;

export const SongsPage = () => {

    const [keyForUpdate, setKeyForUpdate] = useState(0);

    const [isLoading, setIsLoading] = useState(true);

    const [songs, setSongs] = useState([]);
    const [albumNames, setAlbumNames] = useState([]);

    const [isModalOpen, setIsModalOpen] = useState(false);
    const handleOpen = () => setIsModalOpen(true);
    const handleClose = () => {
        setIsModalOpen(false);
        setIsNameError(false);
        setIsDurationError(false);
        setIsAlbumError(false);
    }

    const [pickedSong, setPickedSong] = useState({});

    const [pageNumber, setPageNumber] = useState(0);

    const [isNameError, setIsNameError] = useState(false);
    const [isDurationError, setIsDurationError] = useState(false);
    const [isAlbumError, setIsAlbumError] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        fetch('http://localhost:8080/albums?field=name', {
            method: 'GET'
        })
            .then(response => response.json())
            .then(response => setAlbumNames(response))
            .then(() => {
                fetch('http://localhost:8080/songs', {
                    method: 'GET'
                })
                    .then(response => response.json())
                    .then(response => setSongs(response.sort((a1, a2) => a1.id - a2.id)))
                    .then(() => setIsLoading(false));
            });
    }, [keyForUpdate])

    const handleClick = (song) => {
        setPickedSong(song);
    }

    const handleEditClick = (song) => {
        handleClick(song);
        handleOpen();
    }

    const handleDeleteClick = (album) => {
        fetch('http://localhost:8080/songs', {
            method: 'DELETE',
            body: JSON.stringify(album)
        }).then(() => setKeyForUpdate(keyForUpdate => keyForUpdate + 1));
    }

    const handleAddClick = () => {
        handleClick({
            id: '',
            name: '',
            duration: 0,
            albumName: albumNames[0]
        });
        handleOpen();
    }

    const handleNameChange = (event) => {
        pickedSong.name = event.target.value;
        setPickedSong(pickedSong);
        setIsNameError(checkInput(pickedSong.name));
    }

    const handleDurationChange = (event) => {
        pickedSong['duration'] = getSecondsOutOfTimeString(event.target.value);
        setPickedSong(pickedSong);
        setIsDurationError(pickedSong['duration'] === 0);
    }

    const handleArtistChange = (value) => {
        pickedSong['albumName'] = value;
        setPickedSong(pickedSong);
        setIsAlbumError(checkInput(pickedSong.albumName))
    }

    const checkInputs = () => {
        const name = checkInput(pickedSong.name);
        const duration = pickedSong['duration'] === 0;
        const album = checkInput(pickedSong['albumName']);
        setIsNameError(name);
        setIsDurationError(duration);
        setIsAlbumError(album);
        return name || duration || album;
    }

    const handleSaveClick = () => {
        if (checkInputs()) {
            return;
        }
        fetch('http://localhost:8080/songs', {
            method: pickedSong.id ? 'PUT' : 'POST',
            body: JSON.stringify(pickedSong)
        }).then(() => {
            handleClose();
            setKeyForUpdate(keyForUpdate => keyForUpdate + 1);
        });
    }

    const checkInput = (value) => {
        const regExp = /^.+$/
        return !value.match(regExp);
    }

    const handlePageChange = (event, newPage) => {
        setPageNumber(newPage);
    }

    if (isLoading) {
        return (
            <CircularProgress
                style={{
                    position: 'fixed',
                    left: '50%',
                    top: '50%'
                }}
            />
        );
    }

    return (
        <>
            <StyledBox>
                <HeaderTypography>Songs</HeaderTypography>
                <TableContainer component={Paper} sx={{width: 'auto'}}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>NAME</TableCell>
                                <TableCell>DURATION</TableCell>
                                <TableCell>ALBUM</TableCell>
                                <TableCell>ACTIONS</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {songs.slice(pageNumber * rowsPerPage, pageNumber * rowsPerPage + rowsPerPage)
                                .map((song, index) => {
                                        return (
                                            <TableRow key={song.id}>
                                                <TableCell>{pageNumber * rowsPerPage + index + 1}</TableCell>
                                                <TableCell>{song.name}</TableCell>
                                                <TableCell>{getTimeStringOutOfSeconds(song['duration'])}</TableCell>
                                                <TableCell>{song['albumName']}</TableCell>
                                                <TableCell>
                                                    <ButtonGroup>
                                                        <Button
                                                            onClick={() => handleEditClick(structuredClone(song))}
                                                            variant={'outlined'}
                                                            color={'primary'}
                                                        >Edit</Button>
                                                        <Button
                                                            onClick={() => handleDeleteClick(song)}
                                                            variant={'outlined'}
                                                            color={'error'}
                                                        >Delete</Button>
                                                    </ButtonGroup>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    }
                                )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TableCell colSpan={4}>
                                    <Button
                                        onClick={handleAddClick}
                                        variant={'outlined'}
                                        color={'success'}
                                    >Add</Button>
                                </TableCell>
                                <TablePagination
                                    rowsPerPageOptions={[rowsPerPage]}
                                    count={songs.length}
                                    page={pageNumber}
                                    rowsPerPage={rowsPerPage}
                                    onPageChange={(event, page) => handlePageChange(event, page)}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
                <Modal
                    open={isModalOpen}
                    onClose={handleClose}
                    closeAfterTransition
                >
                    <Fade in={isModalOpen}>
                        <Box sx={FormBoxStyle}>
                            <FormControl error={isNameError} variant={'outlined'} sx={{margin: '10px 0'}}>
                                <InputLabel htmlFor={'name-input'}>Name</InputLabel>
                                <OutlinedInput
                                    id={'name-input'}
                                    label={'Name'}
                                    name={'name-input'}
                                    onChange={handleNameChange}
                                    aria-describedby={'name-input-error-text'}
                                    defaultValue={pickedSong.name}
                                />
                                <FormHelperText
                                    sx={{display: isNameError ? 'block' : 'none'}}
                                    id={'name-input-error-text'}
                                    color={'error'}
                                >The name is incorrect</FormHelperText>
                            </FormControl>
                            <FormControl error={isDurationError} sx={{width: '100%', margin: '10px 0'}}>
                                <OutlinedInput
                                    id={'duration-input'}
                                    type={'time'}
                                    defaultValue={getTimeStringOutOfSeconds(pickedSong['duration'], true)}
                                    onChange={handleDurationChange}
                                />
                                <FormHelperText
                                    sx={{display: isDurationError ? 'block' : 'none'}}
                                >The duration is incorrect</FormHelperText>
                            </FormControl>
                            <FormControl error={isAlbumError} variant={'outlined'} sx={{margin: '10px 0', width: '100%'}}>
                                <Autocomplete
                                    defaultChecked={isModalOpen}
                                    autoHighlight
                                    aria-describedby={'album-input-error-text'}
                                    defaultValue={pickedSong['albumName']}
                                    renderInput={(params) => <TextField error={isAlbumError} {...params} label={'Album'} />}
                                    options={albumNames}
                                    onChange={(event, newValue) => handleArtistChange(newValue)}
                                />
                                <FormHelperText
                                    sx={{display: isAlbumError ? 'block' : 'none'}}
                                    id={'album-input-error-text'}
                                    color={'error'}
                                >The album is incorrect</FormHelperText>
                            </FormControl>
                            <Button onClick={handleSaveClick} variant={'outlined'} color={'success'} sx={{width: '25%'}}>Save</Button>
                        </Box>
                    </Fade>
                </Modal>
            </StyledBox>
        </>
    );
}