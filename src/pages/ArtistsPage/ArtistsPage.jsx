import React, {useEffect, useState} from "react";
import {FormBoxStyle, HeaderTypography, StyledBox} from "../style/Styles.js";
import {
    Box,
    Button,
    ButtonGroup,
    CircularProgress,
    Fade,
    FormControl,
    FormHelperText,
    InputLabel,
    Modal,
    OutlinedInput,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow
} from "@mui/material";
import {getTimeStringOutOfSeconds} from "../../service/time-service";

const rowsPerPage = 10;

export const ArtistsPage = () => {

    const [keyForUpdate, setKeyForUpdate] = useState(0);

    const [isLoading, setIsLoading] = useState(true);

    const [artists, setArtists] = useState([]);
    const [info, setInfo] = useState({infos: []});

    const [isModalOpen, setIsModalOpen] = useState(false);
    const handleOpen = () => setIsModalOpen(true);
    const handleClose = () => {
        setIsModalOpen(false);
        setIsNameError(false);
    }

    const [pickedArtist, setPickedArtist] = useState({});

    const [pageNumber, setPageNumber] = useState(0);

    const [isNameError, setIsNameError] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        fetch('http://localhost:8080/artists', {
            method: 'GET'
        })
            .then(response => response.json())
            .then(response => setArtists(response.sort((a1, a2) => a1.id - a2.id)))
            .then(() => setIsLoading(false))
    }, [keyForUpdate])

    const handleFetchInfoClick = () => {
        fetch('http://localhost:8080/info', {
            method: 'GET'
        })
            .then(response => response.json())
            .then(response => {
                console.log(response);
                setInfo(response);
            })
    }

    const handleClick = (artist) => {
        setPickedArtist(artist);
    }

    const handleEditClick = (artist) => {
        handleClick(artist);
        handleOpen();
    }

    const handleDeleteClick = (artist) => {
        fetch('http://localhost:8080/artists', {
            method: 'DELETE',
            body: JSON.stringify(artist)
        }).then(() => setKeyForUpdate(keyForUpdate => keyForUpdate + 1));
    }

    const handleAddClick = () => {
        handleClick({
            id: '',
            name: ''
        });
        handleOpen()
    }

    const handleNameChange = (event) => {
        pickedArtist.name = event.target.value;
        setPickedArtist(pickedArtist);
        setIsNameError(checkInput(pickedArtist.name));
    }

    const checkInputs = () => {
        const name = checkInput(pickedArtist.name)
        setIsNameError(name);
        return name;
    }

    const handleSaveClick = () => {
        if (checkInputs()) {
            return;
        }
        fetch('http://localhost:8080/artists', {
            method: pickedArtist.id ? 'PUT' : 'POST',
            body: JSON.stringify(pickedArtist)
        }).then(() => {
            handleClose();
            setKeyForUpdate(keyForUpdate => keyForUpdate + 1);
        });

    }

    const checkInput = (value) => {
        const regExp = /^.+$/
        return !value.match(regExp);
    }

    const handlePageChange = (event, newPage) => {
        setPageNumber(newPage);
    }

    if (isLoading) {
        return (
            <CircularProgress
                style={{
                    position: 'fixed',
                    left: '50%',
                    top: '50%'
                }}
            />
        );
    }

    return (
        <>
            <StyledBox>
                <HeaderTypography>Artists</HeaderTypography>
                <TableContainer component={Paper} sx={{width: 'auto'}}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>NAME</TableCell>
                                <TableCell>ALBUMS</TableCell>
                                <TableCell>ACTIONS</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {artists.slice(pageNumber * rowsPerPage, pageNumber * rowsPerPage + rowsPerPage)
                                .map((artist, index) => {
                                    return (
                                        <TableRow key={artist.id}>
                                            <TableCell>{pageNumber * rowsPerPage + index + 1}</TableCell>
                                            <TableCell>{artist.name}</TableCell>
                                            <TableCell>{artist['albums_count']}</TableCell>
                                            <TableCell>
                                                <ButtonGroup>
                                                    <Button
                                                        onClick={() => handleEditClick(structuredClone(artist))}
                                                        variant={'outlined'}
                                                        color={'primary'}
                                                    >Edit</Button>
                                                    <Button
                                                        onClick={() => handleDeleteClick(artist)}
                                                        variant={'outlined'}
                                                        color={'error'}
                                                    >Delete</Button>
                                                </ButtonGroup>
                                            </TableCell>
                                        </TableRow>
                                    );
                                }
                            )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TableCell colSpan={2}>
                                    <Button
                                        onClick={handleAddClick}
                                        variant={'outlined'}
                                        color={'success'}
                                    >Add</Button>
                                </TableCell>
                                <TableCell>
                                    <Button
                                        onClick={handleFetchInfoClick}
                                        variant={'outlined'}
                                        color={'primary'}
                                    >Info</Button>
                                </TableCell>
                                <TablePagination
                                    rowsPerPageOptions={[rowsPerPage]}
                                    count={artists.length}
                                    page={pageNumber}
                                    rowsPerPage={rowsPerPage}
                                    onPageChange={(event, page) => handlePageChange(event, page)}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
                <TableContainer component={Paper} sx={{margin: '10px 0 0 0', width: 'auto', display: info['infos'].length ? 'block' : 'none'}}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>NAME</TableCell>
                                <TableCell>DURATION</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {info['infos'].map(row => {
                                return (
                                    <TableRow key={row.name}>
                                        <TableCell>{row.name}</TableCell>
                                        <TableCell>{getTimeStringOutOfSeconds(row['duration'])}</TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TableCell colSpan={2}>Difference: {getTimeStringOutOfSeconds(info['diff'])}</TableCell>
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
                <Modal
                    open={isModalOpen}
                    onClose={handleClose}
                    closeAfterTransition
                >
                    <Fade in={isModalOpen}>
                        <Box sx={FormBoxStyle}>
                            <FormControl error={isNameError} variant={'outlined'} sx={{margin: '10px 0'}}>
                                <InputLabel htmlFor={'name-input'}>Name</InputLabel>
                                <OutlinedInput
                                    id={'name-input'}
                                    label={'Name'}
                                    name={'name-input'}
                                    onChange={handleNameChange}
                                    aria-describedby={'name-input-error-text'}
                                    defaultValue={pickedArtist.name}
                                />
                                <FormHelperText
                                    sx={{display: isNameError ? 'block' : 'none'}}
                                    id={'name-input-error-text'}
                                    color={'error'}
                                >The name is incorrect</FormHelperText>
                            </FormControl>
                            <Button onClick={handleSaveClick} variant={'outlined'} color={'success'} sx={{width: '25%'}}>Save</Button>
                        </Box>
                    </Fade>
                </Modal>
            </StyledBox>
        </>
    );
}